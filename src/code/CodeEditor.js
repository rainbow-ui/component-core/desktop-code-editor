import CodeMirror from 'codemirror';
import className from 'classnames';
import PropTypes from 'prop-types';
import { Component } from 'rainbowui-desktop-core';
import { Util } from 'rainbow-desktop-tools';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/fold/foldgutter';
import 'codemirror/addon/fold/brace-fold';
import 'codemirror/addon/fold/foldgutter.css';

// hint
import 'codemirror/addon/hint/show-hint';
import 'codemirror/addon/hint/css-hint';
import 'codemirror/addon/hint/html-hint';
import 'codemirror/addon/hint/javascript-hint';
import 'codemirror/addon/hint/sql-hint';
import 'codemirror/addon/hint/xml-hint';
import 'codemirror/addon/display/autorefresh';
import '../css/codemirror-hint.css';

// modes
import 'codemirror/mode/groovy/groovy.js';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/mode/sql/sql.js';
import 'codemirror/mode/xml/xml.js';
import 'codemirror/mode/htmlmixed/htmlmixed.js';
import 'codemirror/mode/markdown/markdown.js';
import 'codemirror/mode/velocity/velocity.js'


export default class CodeEditor extends Component{

    constructor(props) {
        super(props);
        this.state = {
            isFocused: false
        };
    }

    componentDidMount() {
        super.componentDidMount();
        let textareaNode = document.getElementById(this.componentId); //this.refs.textarea;
        let mode = this.props.mode;

        let options = {
            'mode': mode,
            autoRefresh: true, // fix issue: editor is not loading content until clicked.
            'lineNumbers': Util.parseBool(this.props.lineNumbers),
            'lineWrapping': Util.parseBool(this.props.lineWrapping),
            'styleActiveLine': Util.parseBool(this.props.styleActiveLine),
            'foldGutter': Util.parseBool(this.props.foldGutter),
            'gutters': ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
            completeSingle: false
        };

        let showHint = Util.parseBool(this.props.showHint);
        if(showHint && this.props.hintExtraKeys){
            let extraKeys = {};
            extraKeys[this.props.hintExtraKeys] = 'autocomplete';
            options['extraKeys']=extraKeys;
        }

        let cm = this.codeMirror = CodeMirror.fromTextArea(textareaNode, options);

        let value = this.props.value;
        if(this.props.model && this.props.property){
            value = this.props.model[this.props.property]
        }
        cm.setValue(value || '');
        cm.on('change', this.onChange.bind(this));
        cm.on('focus', this.onFocus.bind(this));
        cm.on('blur', this.onBlur.bind(this));

        if(showHint && this.props.hintKeyCode && this.props.hintMap){
            let keyCodes = this.props.hintKeyCode;
            let hintMap = this.props.hintMap;
            let list = [];
            let keyCode = '';
            let hintName = mode + 'Hint';
            cm.on('keyup', function(c, event) {
                if (!c.state.completionActive && /*Enables keyboard navigation in autocomplete list*/
                    hintMap[event.keyCode]) {
                    list = _.map(hintMap[event.keyCode], (data) => data.Name);
                    keyCode = keyCodes[event.keyCode];
                    CodeMirror.commands.autocomplete(c);
                }
            });
            CodeMirror.registerHelper('hint', hintName, function(c, options) {
                let cur = c.getCursor(),
                    token = c.getTokenAt(cur);
                let start = token.start,
                    end = token.end;
                let tokenStr = token.string.trim();
                if (/(^\s*$|\W)/g.test(tokenStr))
                {start = token.start + 1;}
                if (/(&&|\|\|)/g.test(tokenStr))
                {start = token.start + 2;}
                let hhhh = {
                    list: list.filter((e) => keyCode == tokenStr || e.indexOf(tokenStr) === 0),
                    from: CodeMirror.Pos(cur.line, start),
                    to: CodeMirror.Pos(cur.line, end),
                    completeSingle: false
                }
                return hhhh;
            });
            CodeMirror.commands.autocomplete = function(c) {
                CodeMirror.showHint(c, CodeMirror.hint[hintName]);
            };
        } else if (showHint){
            if(mode == 'sql' || mode == 'javascript' || mode == 'xml' || mode == 'html' || mode == 'css'){
                CodeMirror.commands.autocomplete = function(c) {
                    CodeMirror.showHint(c, CodeMirror.hint[mode]);
                };
                cm.on('cursorActivity', function(){
                    cm.showHint( {
                        hint: CodeMirror.hint[mode],
                        completeSingle: false,
                        completeOnSingleClick: false
                    });
                })
            }
        }
    }

    componentWillUnmount() {
        if (this.codeMirror) {
            this.codeMirror.toTextArea();
        }
    }

    renderComponent() {
        const editorClassName = className(
            'ReactCodeMirror',
            this.state.isFocused ? 'ReactCodeMirror--focused' : null,
            this.props.className
        );
        return (
            <div className={ editorClassName } style={ { border: '1px solid #eeeeee' } }>
                <textarea id={ this.componentId } ref="textarea" name={ this.props.path } defaultValue={ '' } autoComplete="off" />
            </div>
        );
    }

    onChange(doc, change) {
        if(change.origin !== 'setValue'){
            if(this.props.model && this.props.property){
                this.props.model[this.props.property] = doc.getValue();
            }
            this.props.onChange && this.props.onChange(doc.getValue());
        }
    }

    onFocus() {
        this.setState({
            isFocused: true
        });
        this.props.onFocus && this.props.onFocus();
    }

    onBlur() {
        this.setState({
            isFocused: true
        });
        this.props.onBlur && this.props.onBlur();
    }

    focus() {
        if (this.codeMirror) {
            this.codeMirror.focus();
        }
    }
}

/**@ignore
 * CodeEditor component prop types
 */
CodeEditor.propTypes =$.extend({}, Component.propTypes, {
    model: PropTypes.object,
    property: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    mode: PropTypes.oneOf(['groovy', 'javascript', 'sql', 'xml', 'html', 'markdown', 'velocity']), // 语言
    lineNumbers: PropTypes.oneOf([PropTypes.string, PropTypes.bool]), // 显示行号
    lineWrapping: PropTypes.oneOf([PropTypes.string, PropTypes.bool]), // 自动换行
    styleActiveLine: PropTypes.oneOf([PropTypes.string, PropTypes.bool]), //
    foldGutter: PropTypes.oneOf([PropTypes.string, PropTypes.bool]), // 展开缩小
    showHint: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    hintExtraKeys: PropTypes.string,
    hintKeyCode: PropTypes.string,
    hintMap: PropTypes.object
});

/**@ignore
 * Get DateTimePicker component default props
 */
CodeEditor.defaultProps = $.extend({}, Component.defaultProps, {
    model: null,
    property: null,
    onChange: () => {
    },
    onBlur: () => {
    },
    onFocus: () => {
    },
    mode: 'javascript',
    lineNumbers: true,
    lineWrapping: true,
    styleActiveLine: true,
    foldGutter: true,
    showHint: false,
    hintExtraKeys: 'Ctrl-Q',
    hintKeyCode: null,
    hintMap: null
});